# Pre-talk
[Last time](https://gitlab.com/danselmi1/SFS/-/tree/main/SIBterraform) I talked here I wrote some markdown
pages that were sort of a hands on class and sort of a presentation. That worked OK but then I saw [Alex
Wise](https://gitlab.com/alexwise/sfs-microservices-talk) wrote some markdown and turned it into HTML that
looked more like slides.

GitLab doesn't render Alex's slides as HTML but there are
[ways](https://stackoverflow.com/questions/55595323/can-i-render-html-on-gitlab) [to
do](https://docs.gitlab.com/ee/user/project/pages/#gitlab-pages-with-static-site-generators-ssgs) that.

Someday I hope I can give a talk here with HTML slides that were easy to make (and view) like Alex did. And
set up a template so it's easy for others to do the same.

# Intro
I'm Dave. I do [Workday](https://www.intecrowd.com). At work I'm a consultant and they gave me business cards
so I could help market our company (or do I mean advertise?). That was about 8 years ago and I still have a
lot of cards left (which haven't been updated for our new branding). I guess I'm better at writing code than
at advertising.🤓

Oh, do you see that emoji? That's an actual character in the markdown text that I wrote--not even a shortcode.
It's the 21st century so things like that Just Work™ because vim, and git, and GitLab all understand Unicode.
They understand it better than I do, so that's not today's talk. (And I needed help to type that character
because a lot of us still use 20th century keyboards.)

Anyway, last month Dr. Haemer was handing out his business card and I was sitting next to David Holz, who I
hand't seen in like 10 years. So I wanted David to send me an email, because he knows things.

> I need to give David my email address. Can I give him my business card?
>
> Do I have any business cards? I don't think so.

Good thing I didn't think so. I had one that was in rough shape.

> It's the 21st century. When was the last time I used a business card (or paper for that matter)?
>
> All my stuff is on my phone. Is there a business card app?
>
> Wait! Last month I had this same conversation. And I fixed it. Like this:

![Dave's contact info](dave.png){width=40%}

So look at that--a 21st century business card.🤓

# Hands On
## First
Install F-Droid. That's one place to get Free Software for Android phones.

![F-Droid download](fdroid.png)

## Next
Open F-Droid and install QR Scanner (PFA). Of the apps I looked at, this one does what we want and seems
useable. It doesn't create other kinds of bar codes, which can be fun, so have a look at what else is out
there.

## Voila
Open the app after it installs and use the menu to go to QR Generator -> VCard. Now you can fill in your data
and save it as a QR code.

![VCard](app.png){width=40%}

# Sharing Data
![XKCD 949](https://imgs.xkcd.com/comics/file_transfer.png)

![XKCD 2194](https://imgs.xkcd.com/comics/how_to_send_a_file.png)

[The ultimate bandwidth: FedEx](https://what-if.xkcd.com/31/)

## Other Ideas?
What else were you using for a business card?

## Other Data?
What other data do you move around?

I was able to move screenshots from my (Android) phone to my (Mac) laptop using Bluetooth. Not quite seamless but close. Of course there's a whole stack of choices and many don't work easily.
