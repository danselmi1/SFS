# Specifying the Docker provider configuration
terraform {
  required_providers {
    # docker isn't from the default registry
    docker = {
      source  = "kreuzwerker/docker"
      version = "3.0.2"
    }
  }
}

provider "docker" {
  host = "unix:///var/run/docker.sock"
}

# images we'll use for our containers
resource "docker_image" "static-site" {
  name = "prakhar1989/static-site"
}

# the container resources
resource "docker_container" "static-site" {
  name = "my_site"
  image = docker_image.static-site.image_id
  publish_all_ports = true
}

