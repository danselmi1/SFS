/* There is an OpenSearch quick start video on YouTube.
 * https://www.youtube.com/watch?v=1xaJODdnnOk
 * Here is a Terraform module to do the same setup without using a mouse.
 *
 * Things the web UI leaves defaulted we probably don't mention.
 */

// the aws provider knows how to build things in AWS; the profile tells it which of my CLI credentials to use
// and that gets me into the right account, default region, etc.
provider "aws" {
  profile = "playground"
}

// "local variables" for values that get repeated; "variables are more complex and can be used across modules
locals {
  domain = "sfs-demo"
  masterUser = "admin"
}

// resource definition for our domain; the values are the type and name of the resource; the name is for TF,
// not the real world
resource "aws_opensearch_domain" "opensearch" {
  domain_name    = "${local.domain}"
  engine_version = "OpenSearch_2.5"

  cluster_config {
    instance_type  = "t3.small.search"
    instance_count = "1"
  }

  ebs_options {
    ebs_enabled = true
    volume_size = "10"
  }

/* if the file ended here it would create an OS domain but it might be hard to use */

  // this argument sets up fine-grained access control
  advanced_security_options {
    enabled                        = true
    anonymous_auth_enabled         = false # must be false for create
    internal_user_database_enabled = true
    // the master user config (uses a random password rather than hard coded)
    master_user_options {
      master_user_name     = "admin"
      master_user_password = random_password.password.result
    }
  }

  // the rest of the arguments are required when advanced security is enabled (TF tells you so)
  encrypt_at_rest {
    enabled = true
  }

  node_to_node_encryption {
    enabled = true
  }

  domain_endpoint_options {
    enforce_https = true
    tls_security_policy = "Policy-Min-TLS-1-2-2019-07"
  }

}

/* in the video, selecting "Only use fine-grained access control" is the same as "Configure domain level
 * access policy" with an allow-all policy; in TF we have to create that as a separate resource
*/

// this is the JSON policy definition translated into TF options for the access_policies argument
data "aws_iam_policy_document" "allow" {
  statement {
    effect = "Allow"

    principals {
      type        = "*"
      identifiers = ["*"]
    }

    actions   = ["es:*"]
    resources = ["${aws_opensearch_domain.opensearch.arn}/*"]
  }
}

// this resource applies the above policy to our domain
resource "aws_opensearch_domain_policy" "opensearch" {
  domain_name     = aws_opensearch_domain.opensearch.domain_name
  access_policies = data.aws_iam_policy_document.allow.json
}

// this resource isn't needed for the video but using it avoids hard coding the master user password
resource "random_password" "password" {
  length  = 32
  special = true
}

// since the password is auto-generated this resource stores it in AWS where we can find it
resource "aws_ssm_parameter" "opensearch_master_user" {
  name        = "/service/${local.domain}/MASTER_USER"
  description = "opensearch_password for ${local.domain} domain"
  type        = "SecureString"
  value       = "${local.masterUser},${random_password.password.result}"
}

// output lets you define values reported at the end of a run; so users will know what values to use (in their
// web browser, say, or as parameters for other modules 
output "endpoint" {
  value = aws_opensearch_domain.opensearch.dashboard_endpoint
}

