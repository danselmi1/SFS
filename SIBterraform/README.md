# Terraform - Small is Beautiful June '23

The material here was written for the [SFS](https://sofree.us/)
[June SIB](https://www.meetup.com/sofreeus/events/293366428/).

The [presentation](presentation.md) links to the other resources used.
