# Intro
I'm Dave. I do [Workday](https://www.intecrowd.com). At work, I also do some AWS instead of Workday. Once upon a time I had to rebuild an
OpenSearch domain used by an EC2 instance. In the process I learned how to use
[Terraform](https://developer.hashicorp.com/terraform/tutorials) and now building my OpenSearch is automatic.
That makes me happy. :smile: I hope that if we do some Terraform together you'll learn something that makes
you happy too.

## Prerequsites
If you want to participate in the hands on part of this presentation you will need:
- Terraform
- Docker
- A computer you can run those on while I talk

## Agenda
Kick off an OpenSearch (AWS) module.

While that runs look at the module.

Look at a Docker module that you can download and try out.

There's a lot to say so we're jumping right in. Details come later for you to read during the break.

# OpenSearch
There's a [YouTube](https://www.youtube.com/watch?v=1xaJODdnnOk
) quick start video that sets up an OpenSearch dashboard. Let's see how to do that without all the pointy clicky parts.

`terraform init`
`terraform plan`
`terraform apply`
…
`terraform destroy`

That's going to take 20-30 (or more?) minutes so while it runs let's look at our [OpenSearch module](OpenSearch/opensearch.tf).

# Docker
So Terraform manages containers, not images. No dockerfiles here.

Download the module from https://gitlab.com/danselmi1/SFS/-/blob/main/SIBterraform/Docker/docker.tf and put it in an empty directory.

Run: 
`terraform init`
`terraform plan`
`terraform apply`

If there are no errors you can run `docker port my_site` and the browse to http://localhost:<port>.

When you're done poking at it use `terraform destroy` to remove the container (etc.)

# OpenSearch File
While we wait for the OpenSearch to finish let's look at its module. If you want to follow along, look at https://gitlab.com/danselmi1/SFS/-/blob/main/SIBterraform/OpenSearch/opensearch.tf.

1. Provider profile
1. Locals vs Variables
1. Basic config
1. Fine-grained access control
1. Policy for public access
1. Recording the user/password
1. Useful output

When the apply completes show:
1. AWS instance
1. SSM parameter
1. Dashboard URL

# Background Material
## Terms
- Module
  - A collection of config files in a directory
  - Terraform reads all the files when it's doing its thing
- Provider
  - The code Terraform uses to talk to a service like AWS, Azure, or that Google thing
- Resource
  - A named type of object that Terraform knows how to create
  - Sort of like a class; you can create multiple instances of a resource (not quite as easy as `new` though)
- Data Source
  - A way to provide documents in your config
- Real World
  - The "world" that exists inside your cloud service provider, such as AWS.

## State
Terraform maintains a state file (well, it might be a file) and expects the real world to conform to it.
Terraform's purpose in life is to create state out of modules, with the side effect of causing the real world
to reflect the state.

Sharing state is something that gets complicated if you're trying to collaborate with others.

If your `terraform apply` sleeps before it's done it might not notice that a resource is created (you know how
long AWS takes to create things). Then if you interrupt it your state will be broken. That will make you sad
:worried:

## Workflow
- `terraform init`
  - Installs providers, modules, etc.
  - Used once with updates when new providers are added
- `terraform plan`
  - Compares the root module to the state and describes the differences
- `terraform apply`
  - Updates the state to match the root module
- `terraform destroy`
  - Updates the state so it is empty

## Resources
[Terraform Tutorials](https://developer.hashicorp.com/terraform/tutorials) and of course Google has a lot of examples of more complex configs.

[OpenSearch TF docs](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/opensearch_domain)

[Terraform + Docker](https://adamtheautomator.com/terraform-docker/) tutorial showing the TF setup (and more ads than you can shake a stick at).

[Docker Getting Started](https://docker-curriculum.com/) a plain Docker tutorial with more interesting containers and no ads.
